import os
import threading
from socket import socket, AF_INET, SOCK_STREAM, SHUT_RDWR

# AF_INET 用于Internet之间的进程通信
# SOCK_STREAM 表示使用TCP协议

# 创建socket对象
serverSocket = socket(AF_INET, SOCK_STREAM)

# 绑定IP地址和端口
strIp = "127.0.0.1"
iPort = 1200
serverSocket.bind((strIp, iPort))

# 开始监听 设置最大监听数量 5
serverSocket.listen(5)
print('-' * 10 + "Server" + '-' * 10)
print("Server Start Listening...")


def HandleClient(clientSocket):
    stopEvent = threading.Event()

    def CloseSocket(Sock):
        if not Sock._closed:
            try:
                # 关闭套接字的读写操作
                Sock.shutdown(SHUT_RDWR)
            except OSError as err:
                print("CloseSocket OSError", err)
            Sock.close()

    # 多线程
    def SendMessagesToClient(clientSocket):

        if clientSocket._closed:
            print("Send:Socket is closed.")
        else:
            print("Send:Socket is not closed. ")

        while not stopEvent.is_set():
            strMessage: str = input()

            if clientSocket._closed:
                print("Send:Socket is closed. Stopping send Messages.")
                break
            else:
                print("Send:Socket is not closed. ")

            try:
                clientSocket.send(strMessage.encode("utf-8"))

                if strMessage == "Disconnect":
                    print("Server Stop Listening...")
                    stopEvent.set()
                    CloseSocket(clientSocket)
                    break

            except OSError as err:
                print("Send OSError", err)
                break

        CloseSocket(serverSocket)
        os._exit(0)

    def ReceiveClientMessages(clientSocket):
        while not stopEvent.is_set():
            try:
                strMessage: str = clientSocket.recv(1024).decode("utf-8")

                if strMessage != "Disconnect":
                    if strMessage != "":
                        print("Client Messages:", strMessage)
                    else:
                        print("Client Messages is None.")
                else:
                    print("Client Disconnect.")
                    stopEvent.set()
                    CloseSocket(clientSocket)
                    break

            except ConnectionAbortedError:
                print("Close Client Connection...")
                break
            except OSError as err:
                print("Receive OSError", err)
                break

    # 创建并启动发送和接收消息的线程
    SendThread = threading.Thread(target=SendMessagesToClient, args=(clientSocket,))
    RecvThread = threading.Thread(target=ReceiveClientMessages, args=(clientSocket,))
    SendThread.start()
    RecvThread.start()
    SendThread.join()
    RecvThread.join()


while True:
    clientSocket, clientAddress = serverSocket.accept()  # 系列解包赋值
    print(f'{clientAddress}' + "Client Connect Successful.")

    clientHandler = threading.Thread(target=HandleClient, args=(clientSocket,))
    clientHandler.start()
